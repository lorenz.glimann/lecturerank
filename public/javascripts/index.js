
if(!localStorage.getItem('id')) {
    localStorage.setItem('id',  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15))
}
const id = localStorage.getItem('id');

function f(choiceindex) {
    return () => {
        console.log('running f');
        const A = document.getElementById("A").innerText;
        const B = document.getElementById("B").innerText;
        axios.post(location.href,  [A, B, choiceindex, id]).then((res) => {
            if(res.status != 200)
                alert("Fehler beim absenden!");
            location.reload();
        })
    };
}

document.getElementById("A").addEventListener("click", f(0));
document.getElementById("B").addEventListener("click", f(1));
document.getElementById("C").addEventListener("click", f(-1));

