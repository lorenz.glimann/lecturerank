const express = require("express");
const router = express.Router();

const lecturenames = [
    "Informatik I",
    "Informatik II",
    "Informatik III",
    "Grundlagen der C-Programmierung",
    "Allgemeines Programmier-Praktikum",
    "Softwaretechnik",
    "Betriebssysteme",
    "Theoretische Informatik",
    "Datenbanken",
    "Telematik / Computer Networks",
    "Mafia 1",
    "Mafia 2",
    "Diskrete Mathematik",
    "Diskrete Stochastik",
    "Algorithms for programming contests",
    "Formale Systeme"
];

let scores = {};
for (let i = 0; i < lecturenames.length; i++) {
    scores[lecturenames[i]] = 0;
}

/* GET home page. */
router.get("/", function (req, res, next) {
    const rand1 = Math.floor(Math.random() * lecturenames.length);
    let rand2;
    do {
        rand2 = Math.floor(Math.random() * lecturenames.length);
    } while (rand1 === rand2);
    //console.log(rand1, rand2, lecturenames[rand1], lecturenames[rand2]);
    res.render("index", {A: lecturenames[rand1], B: lecturenames[rand2]});
});

router.get("/scores", function (req, res, next) {
    res.json(lecturenames.map(name => ([name, scores[name]])));
});

const fs = require('fs');

router.post("/", function (req, res) {
    console.log(req.body);
    fs.appendFile('votes.csv', `${Date.now()};${req.body.join(";")}\n`, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
    const A = req.body[0];
    const B = req.body[1];
    const choice = req.body[2];
    if(choice === 0) {
        console.log(`increasing ${A} from ${scores[A]}`)
        scores[A]++;
        scores[B]--;
    }
    if(choice === 1) {
        scores[A]--;
        scores[B]++;
    }
    res.sendStatus(200);
    /*
    expect a json body with: [A: String, B: String, betterindex: Number]
     */
});

module.exports = router;
